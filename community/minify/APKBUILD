# Maintainer: Wesley van Tilburg <justwesley@protonmail.com>
pkgname=minify
pkgver=2.20.35
pkgrel=0
pkgdesc="Minifier CLI for HTML, CSS, JS, JSON, SVG and XML"
url="https://github.com/tdewolff/minify"
arch="all"
options="net"
license="MIT"
makedepends="go bash"
subpackages="$pkgname-bash-completion"
source="$pkgname-$pkgver.tar.gz::https://github.com/tdewolff/minify/archive/v$pkgver.tar.gz"

export GOCACHE="${GOCACHE:-"$srcdir/go-cache"}"
export GOTMPDIR="${GOTMPDIR:-"$srcdir"}"
export GOMODCACHE="${GOMODCACHE:-"$srcdir/go"}"

build() {
	mkdir build
	go build -o build ./cmd/minify
}

check() {
	go test ./...
}

package() {
	install -Dm755 ./build/minify -t "$pkgdir"/usr/bin
	install -Dm644 ./cmd/minify/bash_completion "$pkgdir"/usr/share/bash-completion/completions/minify
}

sha512sums="
29a0d87a3be380dc1758dc4f3561cd3e11eeac40d4089b14ffaa002895e437bd09b1f44d8a04c4f502f126da7fb19ecbd108256dd6fe7555150d8478b8ca4c2f  minify-2.20.35.tar.gz
"
