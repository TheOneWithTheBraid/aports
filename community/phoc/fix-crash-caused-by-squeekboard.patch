Patch-Source: https://gitlab.gnome.org/World/phosh/phoc/-/merge_requests/574
---
From 12bdc459ffc8277f291f818d9c1042434f845009 Mon Sep 17 00:00:00 2001
From: =?UTF-8?q?Guido=20G=C3=BCnther?= <agx@sigxcpu.org>
Date: Sun, 7 Jul 2024 06:43:35 +0200
Subject: [PATCH 1/2] layer-surface: Rename helper
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit

The actual unmap happens in `handle_unmap`.

Signed-off-by: Guido Günther <agx@sigxcpu.org>
Part-of: <https://gitlab.gnome.org/World/Phosh/phoc/-/merge_requests/574>
---
 src/layer-surface.c | 31 ++++++++++++++++---------------
 src/layer-surface.h |  2 +-
 src/layer_shell.c   |  2 +-
 3 files changed, 18 insertions(+), 17 deletions(-)

diff --git a/src/layer-surface.c b/src/layer-surface.c
index d09c5f10e..dae9fc1ff 100644
--- a/src/layer-surface.c
+++ b/src/layer-surface.c
@@ -122,7 +122,7 @@ phoc_layer_surface_finalize (GObject *object)
   PhocOutput *output = phoc_layer_surface_get_output (self);
 
   if (self->layer_surface->surface->mapped)
-    phoc_layer_surface_unmap (self);
+    phoc_layer_surface_damage (self);
 
   wl_list_remove (&self->link);
   if (output)
@@ -189,27 +189,28 @@ phoc_layer_surface_new (struct wlr_layer_surface_v1 *layer_surface)
 
 
 /**
- * phoc_layer_surface_unmap:
- * @self: The layer surface to unmap
+ * phoc_layer_surface_damage:
+ * @self: The layer surface to damage
  *
- * Unmaps a layer surface
+ * Damage a layer surface
  */
 void
-phoc_layer_surface_unmap (PhocLayerSurface *self)
+phoc_layer_surface_damage (PhocLayerSurface *self)
 {
-  struct wlr_layer_surface_v1 *layer_surface;
+  struct wlr_layer_surface_v1 *wlr_layer_surface;
   struct wlr_output *wlr_output;
 
   g_assert (PHOC_IS_LAYER_SURFACE (self));
-  layer_surface = self->layer_surface;
-
-  wlr_output = layer_surface->output;
-  if (wlr_output != NULL) {
-    phoc_output_damage_whole_surface (wlr_output->data,
-                                      layer_surface->surface,
-                                      self->geo.x,
-                                      self->geo.y);
-  }
+  wlr_layer_surface = self->layer_surface;
+
+  wlr_output = wlr_layer_surface->output;
+  if (!wlr_output)
+    return;
+
+  phoc_output_damage_whole_surface (PHOC_OUTPUT (wlr_output->data),
+                                    wlr_layer_surface->surface,
+                                    self->geo.x,
+                                    self->geo.y);
 }
 
 
diff --git a/src/layer-surface.h b/src/layer-surface.h
index 8374a1540..49ed75d41 100644
--- a/src/layer-surface.h
+++ b/src/layer-surface.h
@@ -48,7 +48,7 @@ struct _PhocLayerSurface {
 };
 
 PhocLayerSurface *phoc_layer_surface_new (struct wlr_layer_surface_v1 *layer_surface);
-void              phoc_layer_surface_unmap (PhocLayerSurface *self);
+void              phoc_layer_surface_damage (PhocLayerSurface *self);
 const char       *phoc_layer_surface_get_namespace (PhocLayerSurface *self);
 PhocOutput       *phoc_layer_surface_get_output (PhocLayerSurface *self);
 void              phoc_layer_surface_set_alpha (PhocLayerSurface *self, float alpha);
diff --git a/src/layer_shell.c b/src/layer_shell.c
index 9f2adbb1d..5ba604b07 100644
--- a/src/layer_shell.c
+++ b/src/layer_shell.c
@@ -854,7 +854,7 @@ handle_unmap (struct wl_listener *listener, void *data)
 
   wl_list_remove (&layer_surface->new_subsurface.link);
 
-  phoc_layer_surface_unmap (layer_surface);
+  phoc_layer_surface_damage (layer_surface);
   phoc_input_update_cursor_focus (input);
 
   if (output)
-- 
GitLab


From 8d0f637c8919410cfa21a43a7c65b507cf930797 Mon Sep 17 00:00:00 2001
From: =?UTF-8?q?Guido=20G=C3=BCnther?= <agx@sigxcpu.org>
Date: Sun, 7 Jul 2024 07:09:00 +0200
Subject: [PATCH 2/2] layer-shell: Mark layer cache dirty when OSK changes
 layers
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit

Fixes: https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=1075857
Fixes: 109b0afe9 ("output: Cache layer surface ordering")

Signed-off-by: Guido Günther <agx@sigxcpu.org>
Tested-by: Andrey Skvortsov <andrej.skvortzov@gmail.com>
Part-of: <https://gitlab.gnome.org/World/Phosh/phoc/-/merge_requests/574>
---
 src/layer_shell.c | 7 +++++++
 1 file changed, 7 insertions(+)

diff --git a/src/layer_shell.c b/src/layer_shell.c
index 5ba604b07..51efefff3 100644
--- a/src/layer_shell.c
+++ b/src/layer_shell.c
@@ -946,6 +946,7 @@ phoc_layer_shell_update_osk (PhocOutput *output, gboolean arrange)
   PhocLayerSurface *osk;
   GSList *seats = phoc_input_get_seats (input);
   gboolean force_overlay = FALSE;
+  enum zwlr_layer_shell_v1_layer old_layer;
 
   g_assert (PHOC_IS_OUTPUT (output));
 
@@ -967,12 +968,18 @@ phoc_layer_shell_update_osk (PhocOutput *output, gboolean arrange)
     }
   }
 
+  old_layer = osk->layer;
   if (force_overlay && osk->layer != ZWLR_LAYER_SHELL_V1_LAYER_OVERLAY)
     osk->layer = ZWLR_LAYER_SHELL_V1_LAYER_OVERLAY;
 
   if (!force_overlay && osk->layer != osk->layer_surface->pending.layer)
     osk->layer = osk->layer_surface->pending.layer;
 
+  if (old_layer != osk->layer) {
+    phoc_output_set_layer_dirty (output, old_layer);
+    phoc_output_set_layer_dirty (output, osk->layer);
+  }
+
   if (force_overlay && arrange)
     phoc_layer_shell_arrange (output);
 }
-- 
GitLab

