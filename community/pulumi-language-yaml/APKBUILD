# Maintainer: Hoang Nguyen <folliekazetani@protonmail.com>
pkgname=pulumi-language-yaml
pkgver=1.9.0
pkgrel=0
pkgdesc="Infrastructure as Code SDK (YAML language provider)"
url="https://pulumi.com/"
# blocked by pulumi
arch="x86_64 aarch64"
license="Apache-2.0"
depends="pulumi"
makedepends="go"
source="$pkgname-$pkgver.tar.gz::https://github.com/pulumi/pulumi-yaml/archive/refs/tags/v$pkgver.tar.gz"
builddir="$srcdir/pulumi-yaml-$pkgver"
# Requires PULUMI_ACCESS_TOKEN for tests to be run
options="!check"

export CGO_ENABLED=0
export GOCACHE="${GOCACHE:-"$srcdir/go-cache"}"
export GOTMPDIR="${GOTMPDIR:-"$srcdir"}"
export GOMODCACHE="${GOMODCACHE:-"$srcdir/go"}"
export GOFLAGS="$GOFLAGS -modcacherw"

build() {
	go build -v \
		-ldflags "-X github.com/pulumi/pulumi-yaml/pkg/version.Version=v$pkgver" \
		-o $pkgname \
		./cmd/pulumi-language-yaml/
}

package() {
	install -Dm755 $pkgname -t "$pkgdir"/usr/bin/
}

sha512sums="
2036896a38fe3d93c8fb989b96ea549f6620b966c0035c284dc85e5ee278a8ac6eefdcee0362d67f4965a05030ab6b67602755a765525bc6fa90cf0ee616d504  pulumi-language-yaml-1.9.0.tar.gz
"
