# Contributor: Carlo Landmeter <clandmeter@alpinelinux.org>
# Maintainer: Carlo Landmeter <clandmeter@alpinelinux.org>
pkgname=ocrmypdf
pkgver=16.4.1
pkgrel=0
pkgdesc="Add OCR text layer to scanned PDF files"
url="https://github.com/ocrmypdf/OCRmyPDF"
# s390x, armhf, x86, ppc64le: tesseract-ocr
# riscv64, loongarch64: tests fails
arch="noarch !s390x !armhf !x86 !ppc64le !riscv64 !loongarch64"
license="MIT"
depends="
	ghostscript
	jbig2enc
	leptonica
	pngquant
	py3-deprecation
	py3-img2pdf
	py3-packaging
	py3-pdfminer
	py3-pikepdf
	py3-pillow
	py3-pluggy
	py3-reportlab
	py3-rich
	python3
	qpdf
	tesseract-ocr
	unpaper
	"
makedepends="
	py3-gpep517
	py3-setuptools_scm
	py3-wheel
	"
checkdepends="
	py3-hypothesis
	py3-pytest
	py3-pytest-cov
	py3-pytest-xdist
	tesseract-ocr-data-eng
	tesseract-ocr-data-osd
	"
subpackages="$pkgname-pyc"
source="https://files.pythonhosted.org/packages/source/o/ocrmypdf/ocrmypdf-$pkgver.tar.gz"

build() {
	export SETUPTOOLS_SCM_PRETEND_VERSION=$pkgver
	gpep517 build-wheel \
		--wheel-dir .dist \
		--output-fd 3 3>&1 >&2
}

check() {
	PYTHONPATH=src \
	pytest
}

package() {
	python3 -m installer -d "$pkgdir" \
		.dist/ocrmypdf*.whl
}

sha512sums="
09a211724a50f3fc3d8c6d3ebdf1f6bfbf56f00b29705f0dba8bc13eca64710c0d6ada7fcf2e7ddd65cf6b04d9339ebaccb32ef4781e7c1dab576118d52f2193  ocrmypdf-16.4.1.tar.gz
"
